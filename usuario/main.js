$(document).ready(function () {
  $("#botao_cadastrar_usuario").on("click", () => {
    var nome = $("#nome").val();
    var email = $("#email").val();
    var senha = $("#senha").val();
    var data_nascimento = $("#data_nascimento").val();
    var doc_rg = $("#doc_rg").val();
    var cpf = $("#cpf").val();
    var telefone_res = $("#telefone_res").val();
    var telefone_cel = $("#telefone_cel").val();
    cadastrarUsuario(nome, email, senha, data_nascimento, doc_rg, cpf, telefone_res, telefone_cel);
    // cadastrarUsuario("Josemar Silva", "josemarsc.dev@gmail.com", "1234", "1996-08-11", "MG17937087", "12312312312", "3335285031", "33988830477");
  });


  /**
  ** EventListenner para detectar quando clicar fora do campo cpf
  ** Valida CPF
  **/
  $("#cpf").focusout(function (e) {
    e.preventDefault();
    var cpf = $("#cpf").val();
    console.log(cpf.length);
    if (cpf.length != 11) {
      console.log("CPF invalido");
      $('#botao_cadastrar_usuario').attr("disabled", "disabled");
    } else {
      $('#botao_cadastrar_usuario').removeAttr("disabled");
    }
  });

  /**
  ** EventListenner para detectar quando clicar fora do campo telefone_res
  ** Valida Telefone Resisdencial
  **/
  $("#telefone_res").focusout(function (e) {
    e.preventDefault();
    var telefone_res = $("#telefone_cel").val();
    if (telefone_res.length < 10 || telefone_res.length > 11) {
      console.log("Telefone residencial invalido");
      $('#botao_cadastrar_usuario').attr("disabled", "disabled");
    } else {
      $('#botao_cadastrar_usuario').removeAttr("disabled");
    }
  });

  /**
  ** EventListenner para detectar quando clicar fora do campo cpf
  ** Valida CPF
  **/
  $("#telefone_cel").focusout(function (e) {
    e.preventDefault();
    var telefone_cel = $("#telefone_cel").val();
    if (telefone_cel.length != 11) {
      console.log("Telefone celular invalido");
      $('#botao_cadastrar_usuario').attr("disabled", "disabled");
    } else {
      $('#botao_cadastrar_usuario').removeAttr("disabled");
    }
  });
});

function cadastrarUsuario(nome, email, senha, data_nascimento, doc_rg, cpf, telefone_res, telefone_cel) {
  console.log(nome, email, senha, data_nascimento, doc_rg, cpf, telefone_res, telefone_cel);
  $.ajax({
    type: "POST",
    url: "http://localhost/eng_soft/travelTeO/database/routes.php",
    data: {
      table: "usuario",
      op: "insert",
      params: {
        insertData: {
          nome: nome,
          email: email,
          senha: converterParaMd5(senha),
          data_nascimento: data_nascimento,
          doc_rg: doc_rg,
          cpf: cpf,
          telefone_cel: telefone_cel,
          telefone_res: telefone_res
        }
      }
    },
    success: function (response) {
      console.log(JSON.parse(response));
      // console.log(response);
    }
  });
}

function converterParaMd5(senha) {
  //conversão 
  return senha;
}
