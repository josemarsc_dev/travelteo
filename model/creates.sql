CREATE DATABASE travelTeo;

USE travelTeo;

CREATE TABLE usuario(
    id INTEGER AUTO_INCREMENT,
    nome TEXT NOT NULL,
    data_nascimento DATE NOT NULL,
    doc_rg VARCHAR(20) NOT NULL UNIQUE,
    cpf CHAR(11) NOT NULL UNIQUE,
    telefone_res CHAR(10) NOT NULL,
    telefone_cel CHAR(11) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    senha CHAR(32) NOT NULL,
    PRIMARY KEY(id)
);



INSERT INTO cliente (nome, data_nascimento, identidade, cpf, telefone_res, telefone_cel, email, senha) VALUES(
    "Josemar Silva",
    "1996-08-11",
    "12123123",
    "12312312312",
    "3333333333",
    "33988888888",
    "josemarsc.dev@gmail.com",
    MD5("josemar")
);