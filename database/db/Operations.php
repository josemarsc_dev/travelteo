<?php

function open_connection()
{
    include_once 'connection/Connection.php';
    $connection = connect();
    return $connection;
}

function select($table, $rows, $conditions)
{
    $sql = 'SELECT ';
    if ($rows) {
        foreach ($rows as $index => $row) {
            $sql .= ($index == 0 ? ($row . ' ') : (', ' . $row));
        }
    } else {
        $sql .= ' * ';
    }
    $sql .= " FROM $table";
    if ($conditions) {
        $i = 0;
        foreach ($conditions as $condition => $value) {
            $sql .= ($i == 0 ? (' WHERE ' . $condition . "'" . $value . "'") : (' AND ' . $condition . "'" . $value . "'"));
            $i++;
        }
    }
    $conn = open_connection();
    $ret = [];
    $stmt = $conn->query($sql);
    while ($row = $stmt->fetch()) {
        array_push($ret, $row);
    }
    $conn = null;
    return $ret;
}

function insert($table, $data)
{
    $rows = '';
    $values = '';
    if ($data) {
        $i = 0;
        foreach ($data as $row => $value) {
            if ($i + 1 == sizeof($data)) {
                $rows .= $row;
                $values .= '"' . $value . '"';
            } else {
                $rows .= $row . ', ';
                $values .= '"' . $value . '", ';
            }
            $i++;
        }
        $sql = "INSERT INTO $table($rows) VALUES($values)";
        $conn = open_connection();
        $ret = [];
        $stmt = $conn->prepare($sql);
        try {
            $stmt->execute();
            $ret[] = $stmt->rowCount();
            $conn = null;
        } catch (PDOException $error) {
            $ret = array('error' => $error);
        }
    } else {
        $ret = array('erro' => 'the parameter data is incomplete');
    }
    return $ret;
}

function update($table, $id, $rows)
{
    $sql = "UPDATE $table SET ";
    $i = 0;
    foreach ($rows as $row => $value) {
        $sql .= ($i == 0 ? (' ' . $row . ' = "' . $value . '"') : (',  ' . $row . ' = "' . $value . '"'));
        $i++;
    }
    $sql .= " WHERE id = '$id'";
    // return $sql;
    $conn = open_connection();
    $ret = [];
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $ret[] = $stmt->rowCount();
    $conn = null;
    return $ret;
}

function delete($table, $id)
{
    $ret = [];
    if ($id) {
        $sql = "DELETE FROM $table WHERE id = $id";
        try {
            $conn = open_connection();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $ret[] = $stmt->rowCount();
            $conn = null;
        } catch (PDOException $erro) {
            $ret = array('error' => $erro);
        }
    } else {
        $ret = array('error' => 'invalid or blank parameter id');
    }
    return $ret;
}
